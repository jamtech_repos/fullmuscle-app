import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { OptionsModel } from '../models/options';
import { ResultadosService } from '../services/resultados.service';

@Component({
  selector: 'app-calculadora',
  templateUrl: './calculadora.component.html',
  styleUrls: ['./calculadora.component.scss']
})
export class CalculadoraComponent implements OnInit {

  @ViewChild('descripcion') public descripcion: ElementRef;
  public paypage = false;
  public succespaypage = false;
  public formfinishpage = false;
  public result: any;
  public tipofinal: number;
  public opciones: OptionsModel;
  public textloader: string;
  public loader: boolean; private page = 1;

  constructor(
    private _resultadoService: ResultadosService
  ) {
    this.opciones = new OptionsModel();
    this.result = null;
    this.tipofinal = 1;
    this.textloader = '';
    this.loader = false;
  }


  ngOnInit() {
  }

  next() {
    if (this.page < 7) {
      switch (this.page) {
        case 1: {
          if (this.opciones.sexo !== '') {
            this.page++;
          }
          break;
        }
        case 2: {
          if (this.opciones.actividad_fisica !== '') {
            this.page++;
         }
         break;
        }
        case 3: {
          if (this.getactiveElement(this.opciones.carnes)) {
            this.page++;
         }
         break;
        }
        case 4: {
          if (this.getactiveElement(this.opciones.alergias)) {
            this.page++;
         }
         break;
        }
        case 5: {
          if (this.getactiveElement(this.opciones.malos_habitos)) {
            this.page++;
         }
         break;
        }
        case 6: {
          if (this.opciones.objetivo !== '') {
            this.page++;
         }
         break;
        }
      }
    }
  }
  back() {
    if (this.page > 1) {
      this.page--;
    }
  }
  public moveToDescripcion() {
    this.descripcion.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'start', inline: 'start' });
  }
  setClassNext() {
      switch (this.page) {
      case 1: {
        if (this.opciones.sexo !== '') {
           return {'enable': true, 'disable': false};
        }
        return {'enable': false, 'disable': true};
      }
      case 2: {
        if (this.opciones.actividad_fisica !== '') {
          return {'enable': true, 'disable': false};
       }
       return {'enable': false, 'disable': true};
      }
      case 3: {
        if (this.getactiveElement(this.opciones.carnes)) {
          return {'enable': true, 'disable': false};
       }
       return {'enable': false, 'disable': true};
      }
      case 4: {
        if (this.getactiveElement(this.opciones.alergias)) {
          return {'enable': true, 'disable': false};
       }
       return {'enable': false, 'disable': true};
      }
      case 5: {
        if (this.getactiveElement(this.opciones.malos_habitos)) {
          return {'enable': true, 'disable': false};
       }
       return {'enable': false, 'disable': true};
      }
      case 6: {
        if (this.opciones.objetivo !== '') {
          return {'enable': true, 'disable': false};
       }
       return {'enable': false, 'disable': true};
      }
    }
  }


  getactiveElement(array: any[]) {
    for (let i = 0; i < array.length; i++) {
      if (array[i].status) {
        return true;
      }
    }
    return false;
  }

  setClassPrevious() {
    let cssClass: { enable: boolean; disable: boolean; };
    if (this.page === 1) {
      cssClass = {
        'enable': false,
        'disable': true,
      };
    } else {
      cssClass = {
        'enable': true,
        'disable': false,
      };
    }
    return cssClass;
  }

  getResults(event) {
    this.opciones.edad = event.edad;
    this.opciones.altura = event.altura;
    this.opciones.peso = event.peso;
    this.opciones.tipo = event.tipo;
    this.activeLoader();
    this._resultadoService.getResults(this.opciones).subscribe(
      response => {
        this.result = response.result;
      }
    );
  }

  iwanttopay(event) {
    this.page = 0;
    this.result.email = event.email;
    this.result.name = event.name;
    this.paypage = true;
  }

  setdataafterpay(event) {
    this.paypage = false;
    this.result.ejercicio = event.ejercicio;
    this.result.tipo_ejercicio = event.tipo_ejercicio;
    if (!this.result.carnes[5].status && this.result.alergias[5].status) {
        /* this.succespaypage = true;
        this.tipofinal = 1;
        this._resultadoService.sendPdfToUser(this.result).subscribe(
          response => {
            console.log(response)
          }
        ) */
        this.formfinishpage = true;
        this.tipofinal = 2;
    } else {
      this.formfinishpage = true;
      this.tipofinal = 2;
    }
  }

  setSuccesspaypage(event) {
    this.activeLoaderFinish();
    const today = new Date().getDate() + '/' + (new Date().getMonth() + 1) + '/' + new Date().getFullYear();
    const data = {
      values: [
        [
          event.formulario.nombre,
          event.formulario.apellido,
          event.formulario.sexo,
          event.formulario.email,
          event.formulario.pais,
          event.formulario.whatsapp,
          event.formulario.objetivo,
          event.formulario.altura_cm,
          event.formulario.peso_kg,
          today,
          ((this.result.ejercicio === true) ? 'Ejercicio agregado' : ''),
          ((event.donacion === true) ? 'Donación' : '')
        ]
      ]
    };
    this._resultadoService.getTokenOAuth().subscribe(
      result => {
        this._resultadoService.sendDataToSheet(data, result).subscribe(
          res => {
            this._resultadoService.sendEmailToAdmin({seleccion: this.result, formulario: event.formulario}).subscribe(
              response => {
                this.loader = false;
                this.formfinishpage = false;
                this.succespaypage = true;
              }
            );
          },
          err => {
            console.log(err);
          }
        );
      }
    );
    /*; */
  }
  activeLoader() {
    let i = 1;
    this.loader = true;
    const interval = setInterval(() => {
        switch (i) {
          case 1: this.textloader = 'Revisando tus opciones'; break;
          case 2: this.textloader = 'Calculando calorias'; break;
          case 3: this.textloader = 'Generando Macronutrientes'; break;
          case 4: this.textloader = 'Realizando recomendaciones'; break;
          case 5: this.textloader = 'Buscando tu peso ideal'; break;
        }
        if (i >= 5) {
          clearInterval(interval);
          this.loader = false;
        }
        i++;
    }, 1000);
  }

  activeLoaderFinish() {
    this.textloader = 'Espere un momento por favor';
    this.loader = true;
  }

  setPositionFooter() {
    if ((this.paypage) || (this.succespaypage)) {
      return {'position-footer1': true, 'position-footer2': false, 'position-footer3': false};
    }
    if (this.page === 1 || this.result !== null) {
      // tslint:disable-next-line:max-line-length
      return {'position-footer1': false, 'position-footer2': true, 'position-footer3': false};
    } else {
      if (this.page === 7) {
        return {'position-footer1': false, 'position-footer2': false, 'position-footer3': true};
      }
      return {'position-footer1': true, 'position-footer2': false, 'position-footer3': false};
    }
  }
  // , -webkit-sticky, -moz-sticky, -ms-sticky, -o-sticky
}
