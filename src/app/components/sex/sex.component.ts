import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-sex',
  templateUrl: './sex.component.html',
  styleUrls: ['./sex.component.scss']
})
export class SexComponent implements OnInit {
  @Input('sexo') sex:string;
  @Output() selectSex = new EventEmitter()
  constructor() { 
    this.sex = '';
  }

  ngOnInit() {
  }

  setSex(sexo) {
    this.sex = sexo
    this.selectSex.emit(sexo);
  }

  setActive(sexo) {
    let cssClass;
    if (this.sex === sexo) {
      cssClass = {
        'active': true
      }
    } else {
      cssClass = {
        'active': false
      }
    }
    return cssClass;
  }

}
