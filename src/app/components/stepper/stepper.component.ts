import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-stepper',
  templateUrl: './stepper.component.html',
  styleUrls: ['./stepper.component.scss']
})
export class StepperComponent implements OnInit {
  @Input('page') page: any
  constructor() { }

  ngOnInit() {
  }

  setActive(i) {
    let cssClass;
    if (this.page === i) {
      cssClass = {
        'step-active':true,
        'step-done':false
      }
    } else {
      if( i < this.page) {
        cssClass = {
          'step-active':false,
          'step-done':true
        }
      } else {
        cssClass = {
          'step-active':false,
          'step-done':false
        }
      }
    }
    return cssClass;
  }
}
