import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-measurements',
  templateUrl: './measurements.component.html',
  styleUrls: ['./measurements.component.scss']
})
export class MeasurementsComponent implements OnInit {
  // tslint:disable-next-line:no-input-rename
  @Input('tipo') type: number;
  // tslint:disable-next-line:no-input-rename
  @Input('edad') edad: number;
  // tslint:disable-next-line:no-input-rename
  @Input('peso') peso: number;
  // tslint:disable-next-line:no-input-rename
  @Input('altura') altura: number;
  @Output() send_data = new EventEmitter();
  constructor() { }

  ngOnInit() {
    this.type = 1;
  }

  scrollTo(id) {
    if (window.innerWidth <= 767) {
    const elmnt = document.getElementById(id);
    elmnt.scrollIntoView(false);
    window.scroll(0, 0);
    }
  }

  setType() {
    if (this.type === 1) {
      if (this.peso) {
        this.peso = Number.parseFloat((this.peso * 2.2046).toFixed(2));
      }
      if (this.altura) {
        this.altura = Math.round(this.altura / 2.54);
      }
      this.type = 2;
    } else {
      if (this.peso) {
        this.peso = Number.parseFloat((this.peso / 2.2046).toFixed(2));
      }
      if (this.altura) {
        this.altura = Number.parseFloat((this.altura * 2.54).toFixed(2));
      }
      this.type = 1;
    }
  }
  setActive(i) {
    if (i === 1) {
      if (this.type === 1) {
        return 'Métrico';
      } else {
        return 'Imperial';
      }
    } else {
      if (this.type === 1) {
        return 'Imperial';
      } else {
        return 'Métrico';
      }
    }
  }

  validateEdad() {
    let cssClass;
    if (this.edad) {
      if (this.edad >= 18 && this.edad < 99) {
        cssClass = {'valid': true, 'danger': false};
      } else {
        cssClass = {'valid': false, 'danger': true};
      }
    } else {
      cssClass = {'valid': false, 'danger': false};
    }
    return cssClass;
  }

  validatePeso() {
    let cssClass;
    if (this.peso) {
      if (this.type === 1) {
        if (this.peso >= 30 && this.peso < 200) {
          cssClass = {'valid': true, 'danger': false};
        } else {
          cssClass = {'valid': false, 'danger': true};
        }
      } else {
        if (this.peso >= 66 && this.peso < 440) {
          cssClass = {'valid': true, 'danger': false};
        } else {
          cssClass = {'valid': false, 'danger': true};
        }
      }
    } else {
      cssClass = {'valid': false, 'danger': false};
    }
    return cssClass;
  }

  validateAltura() {
    let cssClass;
    if (this.altura) {
      if (this.type === 1) {
        if (this.altura >= 139 && this.altura < 220) {
          cssClass = {'valid': true, 'danger': false};
        } else {
          cssClass = {'valid': false, 'danger': true};
        }
      } else {
        if (this.altura >= 55 && this.altura <= 84) {
          cssClass = {'valid': true, 'danger': false};
        } else {
          cssClass = {'valid': false, 'danger': true};
        }
      }
    } else {
      cssClass = {'valid': false, 'danger': false};
    }
    return cssClass;
  }

  setClassFinish() {
    if (this.type === 1) {
      if ((this.peso >= 30 && this.peso < 200) &&
        (this.edad >= 18 && this.edad < 99) &&
        (this.altura >= 139 && this.altura < 220)) {
          return {'done': true};
      } else {
        return {'done': false};
      }
    } else {
      if ((this.peso >= 66 && this.peso < 440) &&
      (this.edad >= 18 && this.edad < 99) &&
      (this.altura >= 55 && this.altura <= 84)) {
        return {'done': true};
      } else {
        return {'done': false};
      }
    }
  }

  sendData() {
    if (this.type === 1) {
      if ((this.peso >= 30 && this.peso < 200) &&
        (this.edad >= 18 && this.edad < 99) &&
        (this.altura >= 139 && this.altura < 220)) {
          this.send_data.emit({edad: this.edad, peso: this.peso, altura: this.altura, tipo: this.type });
        }
      } else {
      if ((this.peso >= 66 && this.peso < 440) &&
        (this.edad >= 18 && this.edad < 99) &&
        (this.altura >= 55 && this.altura <= 84)) {
        this.send_data.emit({edad: this.edad, peso: this.peso, altura: this.altura, tipo: this.type });
      }
    }
  }
  onlyNumbers(evt) {
    const theEvent = evt || window.event;
    let key = theEvent.keyCode || theEvent.which;
    if (key === 38) { // up
      if (this.edad < 99) {
        this.edad++;
      }
      return true;
    }
    if (key === 40) { // down
      if (this.edad > 18) {
        this.edad--;
      }
      return true;
    }
    if (key === 13) {
      this.sendData();
    }
    // tslint:disable-next-line:max-line-length
    if (key === 37 || key === 38 || key === 39 || key === 40 || key === 8 || key === 46) { // Left / Up / Right / Down Arrow, Backspace, Delete keys
      return true;
    }
      key = String.fromCharCode( key );
      const regex = /^[0-9]/;

      if (!regex.test(key)) {
        return false;
      } else {
        return true;
      }
  }

  onlyNumbersWithDecimalAltura(evt) {
    const theEvent = evt || window.event;
    let key = theEvent.keyCode || theEvent.which;
    if (key === 38) { // up
      if (this.altura < 300) {
        this.altura++;
      }
      return true;
    }
    if (key === 40) { // down
      if (this.altura > 130) {
        this.altura--;
      }
      return true;
    }
    if (key === 13) {
      this.sendData();
    }
    // tslint:disable-next-line:max-line-length
    if (key === 37 || key === 38 || key === 39 || key === 40 || key === 8 || key === 46) { // Left / Up / Right / Down Arrow, Backspace, Delete keys
      return true;
    }
    key = String.fromCharCode( key );
    const regex = /^[0-9]*$/;

    if (!regex.test(key)) {
      return false;
    } else {
        return true;
      }
  }

  onlyNumbersWithDecimalPeso(evt) {
    const theEvent = evt || window.event;
    let key = theEvent.keyCode || theEvent.which;
    if (key === 38) { // up
      if (this.peso < 400) {
        this.peso++;
      }
      return true;
    }
    if (key === 40) { // down
      if (this.peso > 30) {
        this.peso--;
      }
      return true;
    }
    if (key === 13) {
      this.sendData();
    }
    // tslint:disable-next-line:max-line-length
    if (key === 37 || key === 38 || key === 39 || key === 40 || key === 8 || key === 46) { // Left / Up / Right / Down Arrow, Backspace, Delete keys
      return true;
    }
    key = String.fromCharCode( key );
    const regex = /^[0-9]*$/;
    if (!regex.test(key)) {
      return false;
    } else {
        return true;
      }
  }
}
