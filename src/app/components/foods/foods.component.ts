import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-foods',
  templateUrl: './foods.component.html',
  styleUrls: ['./foods.component.scss']
})
export class FoodsComponent implements OnInit {
  @Input('carnes') foods: any[]
  @Output() set_carnes = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }

  selectOption(i) {
    switch (i) {
      case 0:{
        this.set_carnes.emit(this.foods)
        if(this.foods[0].status) {
          this.foods[0].status = false;
          break;
        }
        this.foods[0].status = true;
        if(this.foods[5].status) {
          this.foods[5].status = false 
        }
        break;
      }
      case 1:{
        this.set_carnes.emit(this.foods)
        if(this.foods[1].status) {
          this.foods[1].status = false;
          break;
        }
        this.foods[1].status = true;
        if(this.foods[5].status) {
          this.foods[5].status = false 
        }
        break;
      }
      case 2:{
        this.set_carnes.emit(this.foods)
        if(this.foods[2].status) {
          this.foods[2].status = false;
          break;
        }
        this.foods[2].status = true;
        if(this.foods[5].status) {
          this.foods[5].status = false 
        }
        break;
      }
      case 3:{
        this.set_carnes.emit(this.foods)
        if(this.foods[3].status) {
          this.foods[3].status = false;
          break;
        }
        this.foods[3].status = true;
        if(this.foods[5].status) {
          this.foods[5].status = false 
        }
        break;
      }
      case 4:{
        this.set_carnes.emit(this.foods)
        if(this.foods[4].status) {
          this.foods[4].status = false;
          break;
        }
        this.foods[4].status = true;
        if(this.foods[5].status) {
          this.foods[5].status = false 
        }
        break;
      }
      case 5:{
        this.set_carnes.emit(this.foods)
        if(!this.foods[5].status) {
          this.foods[5].status = true
          for (let i = 0; i < this.foods.length -1; i++) {
            this.foods[i].status = false;
          }
          break;
        }
        this.foods[5].status = false; 
        break;
      }
    }
  }

  setActive(i) {
    let cssClass;
    if (this.foods[i].status) {
      cssClass = {
        'option-active':true,
      }
    } else {
      cssClass = {
        'option-active':false,
      }
    }
    return cssClass;
  }
}
