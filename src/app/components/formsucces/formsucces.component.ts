import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { PersonalForm } from 'src/app/models/personalform';

@Component({
  selector: 'app-formsucces',
  templateUrl: './formsucces.component.html',
  styleUrls: ['./formsucces.component.scss']
})
export class FormsuccesComponent implements OnInit {
  public personalform: PersonalForm;
  // tslint:disable-next-line:no-input-rename
  @Input('result') result: any;
  @Output() finishevent = new EventEmitter();
  public otro_enteraste: string;
  public otro_objetivo: string;
  constructor() {
    this.otro_enteraste = '';
    this.otro_objetivo = '';
  }

  ngOnInit() {
    this.personalform = new PersonalForm(this.result);
  }

  scrollTo(id) {
    if (window.innerWidth <= 767) {
    const elmnt = document.getElementById(id);
    elmnt.scrollIntoView();
    window.scroll(100, 100);
    }
  }
  sendForm() {
    if (this.personalform.validateData()) {
      this.finishevent.emit({formulario: this.personalform});
    } else {
      alert('Debe rellenar todos los campos requeridos');
    }
  }

  validateForm() {
    if (this.personalform.validateData()) {
      return {'active': true};
    } else {
      return {'active': false};
    }
  }
}
