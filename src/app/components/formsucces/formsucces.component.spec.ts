import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormsuccesComponent } from './formsucces.component';

describe('FormsuccesComponent', () => {
  let component: FormsuccesComponent;
  let fixture: ComponentFixture<FormsuccesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormsuccesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormsuccesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
