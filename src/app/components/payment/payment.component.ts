import { Component, OnInit, Output, EventEmitter } from '@angular/core';
declare let paypal: any;
@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.scss']
})
export class PaymentComponent implements OnInit {
  @Output() validpayevent = new EventEmitter();
  addScript = false;
  paypalLoad = true;
  finalAmount = 29.95;

  ejercicio = false;
  donation = false;

  tipo_ejercicio: number;
  public loader: boolean;
  constructor() {
    /* this.ejercicio = false; */
    this.tipo_ejercicio = 3;
    this.loader = false;
  }

  paypalConfig = {
    env: 'production',
    client: {
      sandbox: 'Acv8KQRBd3GRFIf4NSUP_wYw_VZu55tAx5GBPh3y89eWgdAoFjLiik21NL7rrOdyqOM56IOQh24j_URz',
      production: 'AXnAVfMHEHTI3Xr8EVONNVChQWHvCbTskBjdIeKIWapL_hdHTT4DMA5WySVZNPRRG42KZTjNF3mXIou-'
    },
    commit: true,
    payment: (data, actions) => {
      return actions.payment.create({
        payment: {
          transactions: [
            { amount: { total: this.finalAmount, currency: 'USD' } }
          ]
        }
      });
    },
    onAuthorize: (data, actions) => {
      this.loader = true;
      return actions.payment.execute().then(( payment) => {
        this.validpayevent.emit({ejercicio: this.ejercicio, tipo_ejercicio: this.tipo_ejercicio, donacion: this.donation});
          this.loader = false;
      });
    }
  };

  ngOnInit() {
  }

  setPriceEjercice() {
    if (this.ejercicio) {
      this.finalAmount -= 19.95;
    } else {
      this.finalAmount += 19.95;
    }
  }

  setDonation() {
    if (this.donation) {
      this.finalAmount -= 2;
    } else {
      this.finalAmount += 2;
    }
  }

  // tslint:disable-next-line:use-life-cycle-interface
  ngAfterViewChecked(): void {
    if (!this.addScript) {
      this.addPaypalScript().then(() => {
        paypal.Button.render(this.paypalConfig, '#paypal-checkout-btn');
        this.paypalLoad = false;
      });
    }
  }

  addPaypalScript() {
    this.addScript = true;
    return new Promise((resolve, reject) => {
      const scripttagElement = document.createElement('script');
      scripttagElement.src = 'https://www.paypalobjects.com/api/checkout.js';
      scripttagElement.onload = resolve;
      document.body.appendChild(scripttagElement);
    });
  }

  setColorSelect() {
    if (this.ejercicio) {
      return  'white';
    } else {
      return  '#3a6ea7';
    }
  }

  setColorSelectDonation() {
    if (this.donation) {
      return  'white';
    } else {
      return  '#3a6ea7';
    }
  }

  up() {
    console.log(this.ejercicio);
    if (this.tipo_ejercicio < 5) {
      this.tipo_ejercicio++;
      if (!this.ejercicio) {
        this.ejercicio = true;
        this.finalAmount += 5;
      }
    }
  }

  down() {
    console.log(this.ejercicio);
    if (this.tipo_ejercicio > 3) {
      this.tipo_ejercicio--;
      if (!this.ejercicio) {
        this.ejercicio = true;
        this.finalAmount += 5;
      }
    }
  }
}
