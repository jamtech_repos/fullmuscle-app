import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-objective',
  templateUrl: './objective.component.html',
  styleUrls: ['./objective.component.scss']
})
export class ObjectiveComponent implements OnInit {
  @Input('objetivo') objetivo : string;
  @Output() set_objetivo = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }
  
  setObjetivo(value : string) {
    this.objetivo = value
    this.set_objetivo.emit(value);
  }

  setActive(value) {
    let cssClass;
    if (this.objetivo === value) {
      cssClass = {
        'active': true
      }
    } else {
      cssClass = {
        'active': false
      }
    }
    return cssClass;
  }
}
