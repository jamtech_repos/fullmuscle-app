import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-bad-habits',
  templateUrl: './bad-habits.component.html',
  styleUrls: ['./bad-habits.component.scss']
})
export class BadHabitsComponent implements OnInit {
  @Input('malos_habitos') bad_habits: any[];
  @Output() set_malos_habitos = new EventEmitter()
  constructor() { }

  ngOnInit() {
  }
  selectOption(i) {
    switch (i) {
      case 0:{
        this.set_malos_habitos.emit(this.bad_habits);
        if(this.bad_habits[0].status) {
          this.bad_habits[0].status = false;
          break;
        }
        this.bad_habits[0].status = true;
        if(this.bad_habits[5].status) {
          this.bad_habits[5].status = false 
        }
        break;
      }
      case 1:{
        this.set_malos_habitos.emit(this.bad_habits);
        if(this.bad_habits[1].status) {
          this.bad_habits[1].status = false;
          break;
        }
        this.bad_habits[1].status = true;
        if(this.bad_habits[5].status) {
          this.bad_habits[5].status = false 
        }
        break;
      }
      case 2:{
        this.set_malos_habitos.emit(this.bad_habits);
        if(this.bad_habits[2].status) {
          this.bad_habits[2].status = false;
          break;
        }
        this.bad_habits[2].status = true;
        if(this.bad_habits[5].status) {
          this.bad_habits[5].status = false 
        }
        break;
      }
      case 3:{
        this.set_malos_habitos.emit(this.bad_habits);
        if(this.bad_habits[3].status) {
          this.bad_habits[3].status = false;
          break;
        }
        this.bad_habits[3].status = true;
        if(this.bad_habits[5].status) {
          this.bad_habits[5].status = false 
        }
        break;
      }
      case 4:{
        this.set_malos_habitos.emit(this.bad_habits);
        if(this.bad_habits[4].status) {
          this.bad_habits[4].status = false;
          break;
        }
        this.bad_habits[4].status = true;
        if(this.bad_habits[5].status) {
          this.bad_habits[5].status = false 
        }
        break;
      }
      case 5:{
        this.set_malos_habitos.emit(this.bad_habits);
        if(!this.bad_habits[5].status) {
          this.bad_habits[5].status = true
          for (let i = 0; i < this.bad_habits.length -1; i++) {
            this.bad_habits[i].status = false;
          }
          break;
        }
        this.bad_habits[5].status = false; 
        break;
      }
    }
  }

  setActive(i) {
    let cssClass;
    if (this.bad_habits[i].status) {
      cssClass = {
        'option-active':true,
      }
    } else {
      cssClass = {
        'option-active':false,
      }
    }
    return cssClass;
  }
}
