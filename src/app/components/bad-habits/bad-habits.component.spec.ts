import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BadHabitsComponent } from './bad-habits.component';

describe('BadHabitsComponent', () => {
  let component: BadHabitsComponent;
  let fixture: ComponentFixture<BadHabitsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BadHabitsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BadHabitsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
