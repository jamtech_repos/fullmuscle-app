import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-physical-activity',
  templateUrl: './physical-activity.component.html',
  styleUrls: ['./physical-activity.component.scss']
})
export class PhysicalActivityComponent implements OnInit {
  // tslint:disable-next-line:no-input-rename
  @Input('actividad_fisica') act_fisica: string;
  @Output() select_actFisica = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }

  selectActFisica(value: string) {
    this.act_fisica = value;
    this.select_actFisica.emit(value);
  }

  setActive(value: string) {
    let cssClass: {};
    if (this.act_fisica === value) {
      cssClass = {
        'active': true
      };
    } else {
      cssClass = {
        'active': false
      };
    }
    return cssClass;
  }
}
