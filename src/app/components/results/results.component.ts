import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef, Renderer } from '@angular/core';
import { ResultadosService } from 'src/app/services/resultados.service';

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.scss']
})

export class ResultsComponent implements OnInit {
  // tslint:disable-next-line:no-input-rename
  @Input('result') result: any;
  @Output() payevent = new EventEmitter();
  @ViewChild('descripcion') public descripcion: ElementRef;
  value = 0;
  public nivel = '';
  public objetivo = '';
  public peso = '';

  public email: string;
  public name: string;
  public gdpr: boolean;
  public capchat: boolean;
  constructor(
    private _resultService: ResultadosService,
    private renderer: Renderer,
    private elem: ElementRef
  ) {
    this.name = '';
    this.email = '';
    this.gdpr = false;
    this.capchat = false;
  }

  ngOnInit() {
    setTimeout(() => {
      this.value = this.result.bmi;
    }, 5000);
    this.nivel = this.setNivel();
    this.objetivo =  this.setObjetivo();
    this.peso = this.setPesoTipo();

    // const elements = this.elem.nativeElement.querySelector('mwl-gauge');
    // tslint:disable-next-line:max-line-length
    // console.log(elements[0]);
    const gauge = document.getElementsByTagName('mwl-gauge');
    document.body.scrollTo(0, 0);
    gauge[0].innerHTML = `<svg viewBox="0 0 100 100" class="gauge2" style="position: absolute">
                            <path class="dial2" fill="none" stroke="#eee" stroke-width="2" d="M 10 50 A 40 40 0 0 1 90 50"></path>
                          </svg>
                          <svg viewBox="0 0 100 100" class="gauge3" style="position: absolute">
                            <path class="dial3" fill="none" stroke="#eee" stroke-width="2" d="M 10 50 A 40 40 0 0 1 90 50"></path>
                          </svg>
                          <svg viewBox="0 0 100 100" class="gauge4" style="position: absolute">
                            <path class="dial4" fill="none" stroke="#eee" stroke-width="2" d="M 10 50 A 40 40 0 0 1 90 50"></path>
                          </svg>`;
  }

  setNivel() {
    switch (this.result.actividad_fisica) {
      case '1': return 'Sedentario';
      case '2': return 'Ejercicio Leve';
      case '3': return 'Ejercicio Moderado';
      case '4': return 'Ejercicio Duro';
      case '5': return 'Atleta';
    }
  }

  setObjetivo() {
    switch (this.result.objetivo) {
      case '1': return 'Aumentar masa muscular';
      case '2': return 'Mantenimiento';
      case '3': return 'Perder grasa corporal';
    }
  }

  setAlturaPies(medida) {
    switch (medida) {
      case 56: return '4ft 8in';
      case 57: return '4ft 8in';
      case 58: return '4ft 8in';
      case 59: return '4ft 8in';
      case 60: return '5ft 0in';
      case 61: return '5ft 1in';
      case 62: return '5ft 2in';
      case 63: return '5ft 3in';
      case 64: return '5ft 4in';
      case 65: return '5ft 5in';
      case 66: return '5ft 6in';
      case 67: return '5ft 7in';
      case 68: return '5ft 8in';
      case 69: return '5ft 9in';
      case 70: return '5ft 10in';
      case 71: return '5ft 11in';
      case 72: return '6ft 0in';
      case 73: return '6ft 1in';
      case 74: return '6ft 2in';
      case 75: return '6ft 3in';
      case 76: return '6ft 4in';
      case 77: return '6ft 5in';
      case 78: return '6ft 6in';
      case 79: return '6ft 7in';
      case 80: return '6ft 8in';
      case 81: return '6ft 9in';
      case 82: return '6ft 10in';
      case 83: return '6ft 11in';
      case 84: return '4ft 0in';
    }
  }

  convertToOz(value) {
    return (value / 28.35).toFixed(1);
  }

  convertToLb(value) {
    return (value * 2.205).toFixed(1);
  }

  convertToLbMPM(value) {
    return Math.round(value * 2.205);
  }

  setColorPeso() {
    if (this.result.bmi < 18.5) {
      return {'color': '#4db2ec'};
    }
    if (this.result.bmi >= 18.5 && this.result.bmi <= 24.9 ) {
      return {'color': '#bcec4d'};
    }
    if (this.result.bmi >= 25 && this.result.bmi <= 29.9) {
      return {'color': '#ecc14d'};
    }
    if (this.result.bmi >= 30) {
      return {'color': '#ec4d4d'};
    }
  }
  setPesoTipo() {
    if (this.result.bmi < 18.5) {
      return 'Bajo peso';
    }
    if (this.result.bmi >= 18.5 && this.result.bmi <= 24.9 ) {
      return 'Peso normal';
    }
    if (this.result.bmi >= 25 && this.result.bmi <= 29.9) {
      return 'Sobrepeso';
    }
    if (this.result.bmi >= 30) {
      return 'Obesidad';
    }
  }

  iwanttopay() {
    if (this.validEmail() && this.name !== '' && this.name.length >= 3 && this.capchat && this.gdpr) {
      this.payevent.emit({email: this.email, name: this.name});
      this._resultService.sendyForm({email: this.email, name: this.name, gdpr: this.gdpr}).subscribe(
        response => {
          this.payevent.emit({email: this.email, name: this.name});
        },
        err => {
          console.log('ERROR SENDY', err);
        }
      );
    }
  }

  public moveToDescripcion() {
    this.descripcion.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'start', inline: 'start' });
  }
  recalculate() {
    window.location.reload();
  }

  setActiveButton() {
    if (this.validEmail() && this.name !== '' && this.name.length >= 3 && this.capchat && this.gdpr) {
      return {
        'active': true
      };
    } else {
      return {
        'active': false
      };
    }
  }

  setCol() {
    if (this.result.sexo === 'hombre') {
      return {
        'col-lg-12': false,
        'col-lg-6': true
      };
    }
    return {'col-lg-6': false, 'col-lg-12': true};
  }
  validateEmail() {
    const regex = /.+\@.+\..+/;
    if (this.email !== '') {
      if (regex.test(this.email)) {
        return {'valid': true, 'danger': false};
      } else {
        return {'valid': false, 'danger': true};
      }
    }
  }

  validateName() {
    const regex = /^[A-Za-z]|[A-Za-z][A-Za-z\s]*[A-Za-z]$/;
    if (this.name.length > 0) {
      if (this.name.length >= 3 && regex.test(this.name)) {
        return {'valid': true, 'danger': false};
      } else {
        return {'valid': false, 'danger': true};
      }
    }
  }

  validName() {
    const regex = /^[A-Za-z]|[A-Za-z][A-Za-z\s]*[A-Za-z]$/;
    if (this.name.length >= 3 && regex.test(this.name)) {
      return {'valid': true, 'danger': false};
    } else {
      return {'valid': false, 'danger': true};
    }
  }

  validEmail() {
    const regex = /.+\@.+\..+/;
    if (this.email !== '' && regex.test(this.email)) {
      return true;
    } else {
      return false;
    }
  }
  public resolved(captchaResponse: any) {
    if (captchaResponse) {
      this.capchat = true;
    } else {
      this.capchat = false;
    }
  }

  public SetPadding() {
    if (this.result.sexo === 'mujer') {
      return {'macro-padding': true};
    }
    return {'macro-padding': false};
  }
}
