import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-allergies',
  templateUrl: './allergies.component.html',
  styleUrls: ['./allergies.component.scss']
})
export class AllergiesComponent implements OnInit {
  @Input('alergias') allergies : any[];
  @Output() set_alergias = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }
  selectOption(i) {
    switch (i) {
      case 0:{
        this.set_alergias.emit(this.allergies);
        if(this.allergies[0].status) {
          this.allergies[0].status = false;
          break;
        }
        this.allergies[0].status = true;
        if(this.allergies[5].status) {
          this.allergies[5].status = false 
        }
        break;
      }
      case 1:{
        this.set_alergias.emit(this.allergies);
        if(this.allergies[1].status) {
          this.allergies[1].status = false;
          break;
        }
        this.allergies[1].status = true;
        if(this.allergies[5].status) {
          this.allergies[5].status = false 
        }
        break;
      }
      case 2:{
        this.set_alergias.emit(this.allergies);
        if(this.allergies[2].status) {
          this.allergies[2].status = false;
          break;
        }
        this.allergies[2].status = true;
        if(this.allergies[5].status) {
          this.allergies[5].status = false 
        }
        break;
      }
      case 3:{
        this.set_alergias.emit(this.allergies);
        if(this.allergies[3].status) {
          this.allergies[3].status = false;
          break;
        }
        this.allergies[3].status = true;
        if(this.allergies[5].status) {
          this.allergies[5].status = false 
        }
        break;
      }
      case 4:{
        this.set_alergias.emit(this.allergies);
        if(this.allergies[4].status) {
          this.allergies[4].status = false;
          break;
        }
        this.allergies[4].status = true;
        if(this.allergies[5].status) {
          this.allergies[5].status = false 
        }
        break;
      }
      case 5:{
        this.set_alergias.emit(this.allergies);
        if(!this.allergies[5].status) {
          this.allergies[5].status = true
          for (let i = 0; i < this.allergies.length -1; i++) {
            this.allergies[i].status = false;
          }
          break;
        }
        this.allergies[5].status = false; 
        break;
      }
    }
  }

  setActive(i) {
    let cssClass;
    if (this.allergies[i].status) {
      cssClass = {
        'option-active':true,
      }
    } else {
      cssClass = {
        'option-active':false,
      }
    }
    return cssClass;
  }
}
