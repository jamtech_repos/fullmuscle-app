import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-succes',
  templateUrl: './succes.component.html',
  styleUrls: ['./succes.component.scss']
})
export class SuccesComponent implements OnInit {
  // tslint:disable-next-line:no-input-rename
  @Input('tipo') tipo: number;
  constructor() { }

  ngOnInit() {
  }

  recalculate() {
    window.location.reload();
  }
}
