import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RecaptchaModule } from 'ng-recaptcha';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { StepperComponent } from './components/stepper/stepper.component';
import { SexComponent } from './components/sex/sex.component';
import { FoodsComponent } from './components/foods/foods.component';
import { AllergiesComponent } from './components/allergies/allergies.component';
import { PhysicalActivityComponent } from './components/physical-activity/physical-activity.component';
import { MeasurementsComponent } from './components/measurements/measurements.component';
import { ObjectiveComponent } from './components/objective/objective.component';
import { BadHabitsComponent } from './components/bad-habits/bad-habits.component';
import { ResultsComponent } from './components/results/results.component';
import { ResultadosService } from './services/resultados.service';
import {HttpClientModule} from '@angular/common/http';
import { GaugeModule } from 'angular-gauge';
import { PaymentComponent } from './components/payment/payment.component';
import { SuccesComponent } from './components/succes/succes.component';
import { FormsuccesComponent } from './components/formsucces/formsucces.component';
import { CalculadoraComponent } from './calculadora/calculadora.component';
import { MenuComponent } from './back/menu/menu.component';
import { RecomendacionesComponent } from './back/recomendaciones/recomendaciones.component';
import { EjercicioComponent } from './back/ejercicio/ejercicio.component';
import { TextoPrincipalComponent } from './components/texto-principal/texto-principal.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    StepperComponent,
    SexComponent,
    FoodsComponent,
    AllergiesComponent,
    PhysicalActivityComponent,
    MeasurementsComponent,
    ObjectiveComponent,
    BadHabitsComponent,
    ResultsComponent,
    PaymentComponent,
    SuccesComponent,
    FormsuccesComponent,
    CalculadoraComponent,
    MenuComponent,
    RecomendacionesComponent,
    EjercicioComponent,
    TextoPrincipalComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    AppRoutingModule,
    GaugeModule.forRoot(),
    RecaptchaModule
  ],
  providers: [ResultadosService],
  bootstrap: [AppComponent]
})
export class AppModule { }
