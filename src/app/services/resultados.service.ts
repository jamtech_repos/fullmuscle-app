import { Injectable } from '@angular/core';
import { GLOBAL } from './global';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { URLSearchParams } from 'url';

@Injectable()
export class ResultadosService {
  private url = GLOBAL.URL;
  constructor(
    private _http: HttpClient) { }

  getResults(data: any): Observable<any> {
    return this._http.post(this.url + 'calculo', data);
  }

  sendyForm(dat: any) {
    const headers = new HttpHeaders()
    .set('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8')
    .set('cache-control', 'no-cache');
    const data = {
      name: dat.name,
      email: dat.email,
      gdpr: dat.gdpr,
      list: '0NM7PdmDc9tzduhnusyuNQ',
      subform: 'yes'
    };
    const body = `name=${dat.name}&email=${dat.email}&gdpr=${dat.gdpr}&list=0NM7PdmDc9tzduhnusyuNQ&subform=yes`;
    return this._http.post('https://fullmusculo.com/sendy2/subscribe', body, {headers, responseType: 'text' as 'json'});
  }

  sendPdfToUser(data: any): Observable<any> {
    return this._http.post(this.url + 'sendpdf', data);
  }

  sendEmailToAdmin(data: any): Observable<any> {
    return this._http.post(this.url + 'sendadminemail', data);
  }

  getTokenOAuth(): Observable<any> {
    const data = {
      client_secret: 'U6ebXSM9HY6-pA2ZW9Q_veeH',
      grant_type: 'refresh_token',
      refresh_token: '1/G6o8z0CYBlWQ9m_8nZC1Rl97l377Z0Lwi_i-5tEOWw-OXxqOBtEtgeveqqRiiMyy',
      client_id: '157475155696-4td8juiqmplqv77j1kl154o0e42i4ann.apps.googleusercontent.com'
    };
    return this._http.post('https://www.googleapis.com/oauth2/v4/token', data);
  }

  sendDataToSheet(data, token): Observable<any> {
    console.log(token);
    const headers = new HttpHeaders()
    .set('Authorization', ' Bearer ' + token.access_token)
    .set('Content-Type', 'application/json')
    .set('cache-control', 'no-cache');
    return this._http.post(
        // tslint:disable-next-line:max-line-length
        `https://sheets.googleapis.com/v4/spreadsheets/1xzn1JPpXYAgXdzP_D6pvuOcM4peCv7GncvBGH9VTXIU/values/A1:append?includeValuesInResponse=false&insertDataOption=INSERT_ROWS&responseDateTimeRenderOption=SERIAL_NUMBER&responseValueRenderOption=FORMATTED_VALUE&valueInputOption=USER_ENTERED&key=AIzaSyCz9-fr9OgsercJM4hlDeLZKDksXXm8A04`,
    data, {headers: headers});

  }
}
