export class OptionsModel {
    public sexo: string;
    public actividad_fisica: string;
    public carnes: any[];
    public alergias: any[];
    public objetivo: string;
    public malos_habitos: any[];
    public tipo: number;
    public edad: number;
    public altura: number;
    public peso: number;

    constructor() {
        this.sexo = '';
        this.actividad_fisica = '';
        this.carnes = [
            {name: 'Pollo', status: false},
            {name: 'Carne', status: false},
            {name: 'Pescado', status: false},
            {name: 'Pavo', status: false},
            {name: 'Cerdo', status: false},
            {name: 'Sin carnes', status: false}
        ];
        this.alergias = [
            {name: 'Soya', status: false},
            {name: 'Lacteos', status: false},
            {name: 'Huevos', status: false},
            {name: 'Nueces', status: false},
            {name: 'Gluten', status: false},
            {name: 'Sin alergias', status: false}
        ];
        this.malos_habitos = [
            {name: 'No dormir lo suficiente', status: false},
            {name: 'Comer muy tarde', status: false},
            {name: 'Mucha sal en las comidas', status: false},
            {name: 'Mucho dulce y bollería', status: false},
            {name: 'Tomar gaseosas', status: false},
            {name: 'Ningun mal habito', status: false}
        ];
        this.tipo = 1;
        this.objetivo = '';
        this.edad = null;
        this.altura = null;
        this.peso = null;
    }
}
