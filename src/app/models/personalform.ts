export class PersonalForm {
    public nombre: string;
    public apellido: string;
    public email: string;
    public como_t_enteraste: string;
    public nombre_dueno_paypal: string;
    public sexo: string;
    public edad: string;
    public pais: string;
    public altura_cm: string;
    public peso_kg: string;
    public peso_min_y_max: string;
    public cintura_cadera: string;
    public por_grasa_corporal: string;
    public cantidad_agua: string;
    public cafe_te: any[];
    public evacuaciones: string;
    public ansiedad: string;
    public entrenamiento: string;
    public desde_entrenamiento: string;
    public tipo_entrenamiento: string;
    public hora_entrenamiento: string;
    public dias_entrenamiento: string;
    public tiempo_entrenamiento: string;
    public objetivo: string;
    public objetivo_explicado: string;
    public comidas_dia: string;
    public frutas_vegetales: string;
    public alimentos_no_consume: string;
    public desayuno: string;
    public merienda_manana: string;
    public almuerzo: string;
    public merienda_tarde: string;
    public cena: string;
    public merienda_noche: string;
    public suplementos: string;
    public cuales_suplementos: string;
    public alergias: string;
    public lesiones: string;
    public comentarios: string;
    public whatsapp: string;

    constructor(data: { name: string; email: string; sexo: string; edad: string; altura: string; peso: string; objetivo: string; }) {
        this.nombre = data.name || '';
        this.apellido = '';
        this.email = data.email || '';
        this.como_t_enteraste = '';
        this.nombre_dueno_paypal = '';
        this.sexo = data.sexo || '';
        this.edad = data.edad || '';
        this.pais = '';
        this.altura_cm = data.altura || '';
        this.peso_kg = data.peso || '';
        this.peso_min_y_max = '';
        this.cintura_cadera = '';
        this.por_grasa_corporal = '';
        this.cantidad_agua = '';
        this.cafe_te = [{value: 'te', status: false}, {value: 'cafe', status: false}, {value: 'ninguno', status: false}];
        this.evacuaciones = '';
        this.ansiedad = '';
        this.entrenamiento = '';
        this.desde_entrenamiento = '';
        this.tipo_entrenamiento = '';
        this.hora_entrenamiento = '';
        this.dias_entrenamiento = '';
        this.tiempo_entrenamiento = '';
        switch (data.objetivo) {
            case '1': {this.objetivo = 'Aumento de masa muscular'; break; }
            case '2': {this.objetivo = 'Mantenimiento de la forma física'; break; }
            case '3': {this.objetivo = 'Perder grasa corporal'; break; }
        }
        this.objetivo_explicado = '';
        this.comidas_dia = '';
        this.frutas_vegetales = '';
        this.alimentos_no_consume = '';
        this.desayuno = '';
        this.merienda_manana = '';
        this.almuerzo = '';
        this.merienda_tarde = '';
        this.cena = '';
        this.merienda_noche = '';
        this.suplementos = '';
        this.cuales_suplementos = '';
        this.alergias = '';
        this.lesiones = '';
        this.comentarios = '';
        this.whatsapp = '';
    }

    validateData() {
        if ((this.nombre !== '') && (this.apellido !== '') && (this.email !== '') &&
        (this.sexo !== '') && (this.edad !== '') && (this.pais !== '') &&
        (this.altura_cm !== '') && (this.peso_kg !== '') && (this.peso_min_y_max !== '') &&
        (this.entrenamiento !== '') && (this.tipo_entrenamiento !== '') && (this.hora_entrenamiento !== '') &&
        (this.dias_entrenamiento !== '') && (this.tiempo_entrenamiento !== '') && (this.objetivo !== '') &&
        (this.objetivo_explicado !== '') && (this.comidas_dia !== '') && (this.suplementos !== '') &&
        (this.alergias !== '') && (this.lesiones !== '') && (this.whatsapp !== '')) {
            return true;
        } else {
            return false;
        }
    }
}
