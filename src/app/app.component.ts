import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {

  location: Location;

  ngOnInit() {
    this.newMethod();
  }

  private newMethod() {
    if (location.protocol === 'http:') {
      window.location.href = location.href.replace('http', 'https');
    }
  }
}
